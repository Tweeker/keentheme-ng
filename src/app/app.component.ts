import {Component, Inject, Renderer2} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'keentheme';
  bShowQuickPanel  = false;

  constructor(private renderer: Renderer2) {

    //console.log(document.body.classList);
  }
  toggleSidebar() {
    if (document.body.classList.contains('kt-aside--minimize') &&
      document.body.classList.contains('kt-aside--minimizing')) {
      this.renderer.removeClass(document.body, 'kt-aside--minimize');
      this.renderer.removeClass(document.body, 'kt-aside--minimizing');
    }else{
      this.renderer.addClass(document.body, 'kt-aside--minimize');
      this.renderer.addClass(document.body, 'kt-aside--minimizing');
    }
  }
  showQuickPanel() {
    this.bShowQuickPanel = !this.bShowQuickPanel;
  }
}
